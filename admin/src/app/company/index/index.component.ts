import {Component, OnInit} from '@angular/core';
import {CompanyService} from "../company.service";
import {SettingsService} from "../../../settings/settings.service";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';


    constructor(public companyService: CompanyService, settings: SettingsService) {
        this.companyService.GetAllCompanies('GetAllCompanies').then((data: any) => {
            console.log("getAllCars : ", data) ,
                this.ItemsArray = data,
                this.ItemsArray1 = data,
                this.host = settings.host,
                console.log(this.ItemsArray[0].logo)
        })
    }

    ngOnInit() {
    }

     DeleteItem(i)
     {
         console.log("Del 1 : " ,  this.ItemsArray[i].id);
         this.companyService.DeleteCompany('DeleteCompany', this.ItemsArray[i].id).then((data: any) => {this.ItemsArray = data , console.log("Del 2 : " ,  data);})
     }
    
    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function(d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

}
