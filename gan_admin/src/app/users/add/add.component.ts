import {Component, OnInit, ElementRef, ChangeDetectionStrategy , ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";

import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

    public navigateTo:string = '/users/index';
    public paramsSub;
    public id: number;
    public imageSrc: string = '';
    public folderName:string = 'users';
    public rowsNames:any[] = ['שם מלא','כתובת','טלפון','אימייל','סיסמה'];
    public rows:any[] = ['name','address','phone','mail','password']; //description logo image

    @ViewChild("logo") Logo;
    @ViewChild("image") Image;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router) {
        console.log("Row : " , this.rows)
    }

    onSubmit(form:NgForm)
    {
        let fi1 = this.Image.nativeElement;
        let fileToUpload1;
        if (fi1.files && fi1.files[0]) {fileToUpload1 = fi1.files[0];}

        this.service.AddItem('addUser',form.value,fileToUpload1).then((data: any) => {
            console.log("addUser : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }
    
    ngOnInit() {
        this.paramsSub = this.route.params.subscribe(params => this.id = params['id']);
    }
    
    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }

}
