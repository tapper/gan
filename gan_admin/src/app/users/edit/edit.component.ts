import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  {

    public Id;
    public navigateTo:string = '/users/index';
    public imageSrc: string = '';
    public Items:any[]=[];
    public rowsNames:any[] = ['שם מלא','כתובת','טלפון','אימייל','סיסמה'];
    public rows:any[] = ['name','address','phone','mail','password']; //description logo image
    public Item;
    public Logo1;
    public Image1;
    public host;
    public Change:boolean = false;
    public Change1:boolean = false;
    @ViewChild("image") Image;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, public settings: SettingsService) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];
            this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            this.Logo1 = this.host+''+this.Item.logo;
            this.Image1 = this.host+''+this.Item.image;
            console.log("fkitchens " ,this.Item )
        });
    }

    onSubmit(form:NgForm)
    {
        let fi1 = this.Image.nativeElement;
        let fileToUpload1;
        if (fi1.files && fi1.files[0]) {fileToUpload1 = fi1.files[0]; console.log("fff1 : ",fileToUpload1);}
        this.Item.change1 = this.Change1;


        console.log("Edit : " , this.Item);
        this.service.EditItem('EditUser',this.Item,fileToUpload1).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }

    onChange(event) {
        this.Change = true;
        var files = event.srcElement.files;
        this.Logo1 = event.target.files[0];
       // this.Logo1 = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.Logo1 = e.target.result;
           // this.Logo1 = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }

    onChange1(event) {
        this.Change1 = true;
        var files = event.srcElement.files;
        this.Image1 = event.target.files[0];


        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.Image1 = e.target.result;
          //  this.Image = this.Image1;
           // this.Image1 = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
}
