import {Component, OnInit, ElementRef, ChangeDetectionStrategy , ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";

import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

    public navigateTo:string = '/comp/index';
    public paramsSub;
    public id: number;
    public imageSrc: string = '';
    public folderName:string = 'category';
    public rowsNames:any[] = ['שם החברה','כתובת','טלפון','אימייל','WAZE','אתר אינטרנט','פייסבוק','סיסמה','subdomain'];
    public rows:any[] = ['name','address','phone','email','waze','website','facebook','password','subdomain']; //description logo image

    @ViewChild("logo") Logo;
    @ViewChild("image") Image;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router) {
        console.log("Row : " , this.rows)
    }

    onSubmit(form:NgForm)
    {
        console.log(form.value);
        let fi = this.Logo.nativeElement;
        let fileToUpload;
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}

        let fi1 = this.Image.nativeElement;
        let fileToUpload1;
        if (fi1.files && fi1.files[0]) {fileToUpload1 = fi1.files[0];}

        this.service.AddItem('AddCompany1',form.value,fileToUpload,fileToUpload1).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }
    
    ngOnInit() {
        this.paramsSub = this.route.params.subscribe(params => this.id = params['id']);
    }
    
    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }

}
