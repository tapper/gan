import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent  {

    public Id;
    public navigateTo:string = '/regions/index';
    public imageSrc: string = '';
    public Items:any[]=[];
    public rowsNames:any[] = ['שם איזור'];
    public rows:any[] = ['name']; //description private image
    public Item;
    public host;
    public Image;
    public Change:boolean = false;
    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router, public settings: SettingsService) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];
            this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            this.Image = this.host+''+this.Item.logo;
            console.log("fkitchens " ,this.Item )
        });
    }

    onSubmit(form:NgForm)
    {
        this.service.EditItem('EditRegion',this.Item).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });
    }

    onChange(event) {
        this.Change = true;
        var files = event.srcElement.files;
        this.Image = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.Image = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
}
