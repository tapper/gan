webpackJsonp([9],{

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//const ServerUrl = "http://tapper.co.il/647/laravel/public/api/";
var ChatService = (function () {
    function ChatService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ChatArray = [];
        this._ChatArray = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["Subject"]();
        this.ChatArray$ = this._ChatArray.asObservable();
        this.ServerUrl = Settings.ServerUrl;
        console.log(this.ChatArray);
    }
    ChatService.prototype.addTitle = function (url, obj) {
        var body = 'info=' + JSON.stringify(obj);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) { console.log("Chat : ", data); }).toPromise();
    };
    ChatService.prototype.getChatDetails = function (url, reciverId) {
        var _this = this;
        var body = new FormData();
        body.append('sender_id', window.localStorage.id);
        body.append('reciver_id', reciverId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.updateChatArray(data); }).toPromise();
    };
    ChatService.prototype.updateChatArray = function (data) {
        this.ChatArray = data;
        //this._ChatArray.next({ text: this.ChatArray });
    };
    ChatService.prototype.pushToArray = function (data) {
        this.ChatArray.push(data);
        console.log("Push1 : ", data);
        this._ChatArray.next(this.ChatArray);
    };
    ChatService.prototype.registerPush = function (url, pushid) {
        var body = new FormData();
        body.append('push_id', pushid.toString());
        body.append('user_id', window.localStorage.identify.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return ''; }).do(function (data) { console.log("chat:", data); });
    };
    ChatService.prototype.getChatMessagesCount = function (url) {
        var body = new FormData();
        body.append('user_id', window.localStorage.identify.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { return data; }).toPromise();
    };
    ChatService.prototype.getMessages = function (url) {
        var body = new FormData();
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { return data; }).toPromise();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["d" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["d" /* Content */])
    ], ChatService.prototype, "content", void 0);
    ChatService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], ChatService);
    return ChatService;
}());

;
//# sourceMappingURL=chatService.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JobsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var JobsService = (function () {
    function JobsService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = Settings.ServerUrl;
    }
    ;
    JobsService.prototype.addJob = function (url, info) {
        var body = 'uid=' + window.localStorage.id + '&info=' + JSON.stringify(info);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) { console.log("SaveMeals : ", data); }).toPromise();
    };
    JobsService.prototype.getAllJobs = function (url) {
        var body = new FormData();
        body.append('id', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    JobsService.prototype.getAllOpenJobs = function (url) {
        var body = new FormData();
        body.append('id', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    JobsService.prototype.UpdateJobState = function (url, id, state) {
        var body = new FormData();
        body.append('id', id);
        body.append('state', state);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    JobsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], JobsService);
    return JobsService;
}());

;
//# sourceMappingURL=JobsService.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProjectBidsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ProjectBidsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProjectBidsPage = (function () {
    function ProjectBidsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Bids = [];
        this.Bids = this.navParams.get('project');
        console.log(this.Bids);
        console.log('ProjectBidsPage');
    }
    ProjectBidsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProjectBidsPage');
    };
    ProjectBidsPage.prototype.goBack = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */], { SelectedIndex: 1 });
    };
    ProjectBidsPage.prototype.gotoChat = function (i) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__chat_chat__["a" /* ChatPage */], { id: this.Bids[i].user[0].id, name: this.Bids[i].user[0].name, type: 1 });
    };
    ProjectBidsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-project-bids',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\project-bids\project-bids.html"*/'<!--\n\n  Generated template for the ProjectBidsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation. navPop\n\n-->\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n\n\n  <ion-navbar hideBackButton >\n\n    <ion-title align="center" class="HeaderTitle"> צהלה <span class="HeaderSubTitle">הצעות לפרוייקט</span> </ion-title>\n\n    <ion-buttons left>\n\n      <button ion-button (click)="goBack()" icon-only>\n\n        <ion-icon ios="ios-arrow-back" md="md-arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div>\n\n    <ion-list>\n\n      <ion-item  text-wrap detail-push *ngFor="let bid of Bids let i=index" side="right" style="direction:rtl">\n\n        <div class="jobRight">\n\n          <img src="images/i1.png" style="width: 100%" />\n\n        </div>\n\n        <div class="jobLeft">\n\n          <h2>{{bid.user[0].name}}</h2>\n\n          <h3>{{bid.note[0].info}}</h3>\n\n          <button ion-button style="direction: rtl" (click)="gotoChat(i)">שלחי הודעה ל{{bid.user[0].name}}</button>\n\n        </div>\n\n      </ion-item>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\project-bids\project-bids.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ProjectBidsPage);
    return ProjectBidsPage;
}());

//# sourceMappingURL=project-bids.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OpenJobsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_JobsService__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_sent_to_server_service__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the OpenJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OpenJobsPage = (function () {
    function OpenJobsPage(navCtrl, navParams, jobsService, Settings, serverService, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.jobsService = jobsService;
        this.Settings = Settings;
        this.serverService = serverService;
        this.alertCtrl = alertCtrl;
        this.CurrentJob = '';
        this.Notes = [];
        this.jobsService.getAllOpenJobs('getAllOpenJobs').then(function (data) {
            console.log("Jobs : ", data);
            _this.jobs = data;
            Settings.openJobs = data.length;
        });
        this.serverService.getAllNotes('getAllNotes').then(function (data) {
            _this.Notes = data;
        });
    }
    OpenJobsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OpenJobsPage');
    };
    OpenJobsPage.prototype.presentPrompt = function (i) {
        var _this = this;
        console.log("JB ", i, this.jobs[i].id);
        this.CurrentJob = this.jobs[i].id;
        var myAlert = this.alertCtrl.create({
            title: "בחרי תבנית",
            cssClass: 'noteAlert',
            buttons: [
                {
                    text: 'בטלי',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'הגישי הצעה',
                    handler: function (data) {
                        _this.serverService.addBid('addBid', _this.CurrentJob, _this.Notes[data].id).then(function (data) {
                            _this.jobs[i].isBid = 1;
                        });
                    }
                }
            ]
        });
        for (var i_1 = 0; i_1 < this.Notes.length; i_1++) {
            myAlert.addInput({
                type: 'radio',
                label: this.Notes[i_1]['title'],
                value: i_1.toString()
            });
        }
        myAlert.present();
    };
    OpenJobsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-open-jobs',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\open-jobs\open-jobs.html"*/'<!--\n\n  Generated template for the OpenJobsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n  <ion-navbar >\n\n    <button class="HeaderMenuButton"  ion-button icon-only menuToggle end>\n\n      <ion-icon name="md-menu"></ion-icon>\n\n    </button>\n\n    <ion-title align="center" class="HeaderTitle"> צהלה <span class="HeaderSubTitle">המשרות שלי</span> </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div>\n\n    <ion-list>\n\n      <ion-item  text-wrap detail-push *ngFor="let job of jobs let i=index" side="right" style="direction:rtl">\n\n        <div class="jobRight">\n\n          <img class="thumb" src="https://wordsmith.org/words/images/avatar2_large.png" style="width: 100%" />\n\n        </div>\n\n        <div class="jobLeft">\n\n          <div class="time">\n\n            <div class="timeLeft">\n\n              <h3> תאריך : {{job.Date2}}</h3>\n\n            </div>\n\n            <div class="timeRight">\n\n              <h3 style="direction: rtl">שעה : {{job.Time2}}</h3>\n\n            </div>\n\n          </div>\n\n          <hr style="margin:0px;">\n\n          <h2>{{job.info}}</h2>\n\n\n\n          <button *ngIf="job.isBid == 0" ion-button (click)="presentPrompt(i)">הגישי הצעה</button>\n\n          <button *ngIf="job.isBid != 0" ion-button>נשלחה הצעה לעבודה</button>\n\n        </div>\n\n      </ion-item>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\open-jobs\open-jobs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_JobsService__["a" /* JobsService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_4__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], OpenJobsPage);
    return OpenJobsPage;
}());

//# sourceMappingURL=open-jobs.js.map

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_loginService__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_toastService__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_diagnostic__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_file__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_path__ = __webpack_require__(323);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, Login, Toast, camera, diagnostic, transfer, file, filePath, actionSheetCtrl, toastCtrl, platform, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Login = Login;
        this.Toast = Toast;
        this.camera = camera;
        this.diagnostic = diagnostic;
        this.transfer = transfer;
        this.file = file;
        this.filePath = filePath;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.info = {
            name: '',
            phone: '',
            address: '',
            info: '',
            password: '',
            mail: '',
            image: '',
            type: '',
            area: ''
        };
        this.Regions = [];
        this.Types = [];
        this.lastImage = null;
        this.options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.Login.getRegions('GetRegions').then(function (data) {
            console.log("Regions : ", data);
            _this.Regions = data;
        });
        this.Login.GetTypes('GetTypes').then(function (data) {
            console.log("Types : ", data);
            _this.Types = data;
        });
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.doRegister = function () {
        var _this = this;
        console.log("register response1: ");
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.info['name'].length < 3)
            this.Toast.presentToast('הכנס שם מלא');
        else if (this.info['phone'].length < 8)
            this.Toast.presentToast('הכנס מספר טלפון חוקי');
        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין');
        }
        else if (this.info['address'].length < 2)
            this.Toast.presentToast('הכנס כתובת חוקית');
        else if (this.info['password'].length < 3)
            this.Toast.presentToast('הכנס סיסמה תקינה');
        else if (this.info['area'] == '')
            this.Toast.presentToast('בחר איזור');
        else if (this.info['type'] == '')
            this.Toast.presentToast('בחר סוג תפקיד');
        else {
            this.Login.RegisterUser("RegisterUser", this.info).then(function (data) {
                if (data == 0) {
                    _this.Toast.presentToast('ארעה שגיאה , אנא נסה שנית');
                }
                else {
                    window.localStorage.id = data;
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
                }
            });
        }
    };
    RegisterPage.prototype.GoToLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
    };
    /// Image Upload
    RegisterPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    RegisterPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 60,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 1000,
            allowEdit: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            console.log("f0");
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                console.log("f1");
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    _this.correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    _this.currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(_this.correctPath, _this.currentName, _this.createFileName());
                });
            }
            else {
                console.log("f1");
                _this.currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                console.log("f2 : ", _this.currentName);
                _this.correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                console.log("f3 : ", _this.correctPath);
                _this.copyFileToLocalDir(_this.correctPath, _this.currentName, _this.createFileName());
                console.log("f4");
            }
        }, function (err) {
            _this.presentToast('Error while selecting image.');
        });
    };
    // Create a new name for the image
    RegisterPage.prototype.createFileName = function () {
        console.log("f1");
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    RegisterPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        console.log("f2 : " + namePath + " : " + currentName + " : " + newFileName);
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.lastImage = newFileName;
            _this.uploadImage();
        }, function (error) {
            _this.presentToast('Error while storing file.');
        });
    };
    RegisterPage.prototype.presentToast = function (text) {
        console.log("f3");
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    RegisterPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    RegisterPage.prototype.uploadImage = function () {
        var _this = this;
        // Destination URL
        console.log("Up1  : ");
        var url = "http://www.tapper.co.il/gan/laravel/public/api/GetFile";
        // File for Upload
        console.log("Up2  : " + this.lastImage + " : " + this.pathForImage(this.lastImage));
        var targetPath = this.pathForImage(this.lastImage);
        // File name only
        var filename = this.lastImage;
        var options = {
            fileKey: "file",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: { 'fileName': filename }
        };
        var fileTransfer = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.loading.present();
        // Use the FileTransfer to upload the image
        console.log("Up1  : ", targetPath + " : " + url + " : " + options);
        fileTransfer.upload(targetPath, url, options).then(function (data) {
            console.log("Updata  : ", data.response);
            _this.loading.dismissAll();
        }, function (err) {
            _this.loading.dismissAll();
            _this.presentToast('Error while uploading file.');
        });
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\register\register.html"*/'<ion-content padding>\n\n  <div class="loginMain">\n\n    <div class="loginTitle">\n\n      הרשמה\n\n    </div>\n\n    <div class="InputsText" align="center">\n\n      <div class="InputText">\n\n        <input type="text" placeholder="הכנס שם מלא" [(ngModel)]="info.name" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס מספר טלפון" [(ngModel)]="info.phone" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס כתובת מדוייקת" [(ngModel)]="info.address" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס אימייל" [(ngModel)]="info.mail" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס סיסמה" [(ngModel)]="info.password" />\n\n      </div>\n\n\n\n      <ion-item class="selectRegion">\n\n        <ion-label>בחר איזור</ion-label>\n\n        <ion-select [(ngModel)]="info.area" >\n\n          <ion-option value="{{region.id}}" *ngFor="let region of Regions">{{region.name}}</ion-option>\n\n        </ion-select>\n\n      </ion-item>\n\n\n\n      <ion-item class="selectRegion">\n\n        <ion-label>בחר סוג</ion-label>\n\n        <ion-select [(ngModel)]="info.type">\n\n          <ion-option value="{{type.id}}" *ngFor="let type of Types">{{type.name}}</ion-option>\n\n        </ion-select>\n\n      </ion-item>\n\n\n\n      <div class="InputText mt10">\n\n        <textarea [(ngModel)]="info.info" name="info" placeholder="הכניסי קצת פרטים עלייך" rows="4"></textarea>\n\n      </div>\n\n\n\n      <div style="width: 100%; margin-top: 20px;" align="center" (click)="presentActionSheet()">\n\n        <div style="width: 90%">\n\n          <button ion-button color="primary" style="width: 100%; color: black">הוסף תמונה</button>\n\n        </div>\n\n      </div>\n\n\n\n      <button class="loginButton" ion-button block (click)="doRegister()">התחל</button>\n\n      <a class="register" (click)="GoToLogin()">חזור להתחברות  </a>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_4__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_diagnostic__["a" /* Diagnostic */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_9__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_path__["a" /* FilePath */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* LoadingController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_add_note_modal_add_note_modal__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_sent_to_server_service__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the NotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NotesPage = (function () {
    function NotesPage(navCtrl, navParams, modalCtrl, serverService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.serverService = serverService;
        this.Notes = [];
        this.getAllNotes();
    }
    NotesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotesPage');
    };
    NotesPage.prototype.openModal = function () {
        var _this = this;
        var noteModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_add_note_modal_add_note_modal__["a" /* AddNoteModalComponent */]);
        noteModal.present();
        noteModal.onDidDismiss(function (data) {
            if (data.type == 1) {
                _this.serverService.addNote('addNote', data).then(function (data) {
                    _this.Notes = data;
                });
            }
        });
    };
    NotesPage.prototype.updateModal = function (i) {
        var _this = this;
        var noteModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_add_note_modal_add_note_modal__["a" /* AddNoteModalComponent */], {
            title: this.Notes[i]['title'],
            info: this.Notes[i]['info']
        });
        noteModal.present();
        noteModal.onDidDismiss(function (data) {
            _this.serverService.updateNote('updateNote', data, _this.Notes[i]['id']).then(function (data) {
                _this.Notes = data;
            });
        });
    };
    NotesPage.prototype.getAllNotes = function () {
        var _this = this;
        this.serverService.getAllNotes('getAllNotes').then(function (data) {
            _this.Notes = data;
        });
    };
    NotesPage.prototype.deleteNote = function (id) {
        var _this = this;
        this.serverService.deleteNote('deleteNote', id).then(function (data) {
            _this.Notes = data;
        });
    };
    NotesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notes',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\notes\notes.html"*/'<!--\n\n  Generated template for the NotesPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation. (click)="deleteNote(note.id)"\n\n-->\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n\n\n    <ion-navbar>\n\n        <button class="HeaderMenuButton" ion-button icon-only menuToggle end>\n\n            <ion-icon name="md-menu"></ion-icon>\n\n        </button>\n\n        <ion-title align="center" class="HeaderTitle"> צהלה <span class="HeaderSubTitle">תבניות</span></ion-title>\n\n        <ion-buttons>\n\n            <button ion-button icon-only (click)="openModal()" class="HeaderLeftButton">\n\n                <ion-icon name="ios-add-circle-outline"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <button ion-button block (click)="openModal()"> הוסיפי תבנית</button>\n\n\n\n    <div>\n\n        <ion-list>\n\n            <ion-item text-wrap detail-push *ngFor="let note of Notes let i=index " side="right" style="direction:rtl">\n\n\n\n                <div class="time">\n\n                    <div class="timeLeft">\n\n                        <h3>  {{note.title}}</h3>\n\n                        <h2>{{note.info}}</h2>\n\n                    </div>\n\n                    <div class="timeRight" >\n\n                        <img src="images/del.png" style="width:48%" class="deleteIcon" (click)="deleteNote(note.id)"/>\n\n                        <!--<ion-icon name="ios-create-outline" (click)="updateModal(i)" class="deleteIcon"></ion-icon>-->\n\n                        <!--<ion-icon name="ios-trash-outline" (click)="deleteNote(note.id)" class="deleteIcon"></ion-icon>-->\n\n                    </div>\n\n                    <div class="timeRight" >\n\n                        <img src="images/edit.png" style="width:48%; margin-left:-10px;" class="deleteIcon" (click)="updateModal(i)"/>\n\n                    </div>\n\n                </div>\n\n            </ion-item>\n\n        </ion-list>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\notes\notes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__services_sent_to_server_service__["a" /* sent_to_server_service */]])
    ], NotesPage);
    return NotesPage;
}());

//# sourceMappingURL=notes.js.map

/***/ }),

/***/ 178:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 178;

/***/ }),

/***/ 22:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserId = "http://tapper.co.il/647/laravel/public/api/";
var Config = (function () {
    function Config() {
        this.UserId = '';
        this.PushId = '';
        this.ServerUrl = 'http://tapper.co.il/gan/laravel/public/api/';
        this.CourseId = '';
        this.Substitute = 0;
        this.JobsCount = 0;
        this.openJobs = 0;
        this.Messages = 0;
    }
    ;
    Config.prototype.SetUserPush = function (push_id) {
        this.PushId = push_id;
        window.localStorage.push_id = push_id;
    };
    Config = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], Config);
    return Config;
}());

;
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 223:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/all-substitute/all-substitute.module": [
		698,
		8
	],
	"../pages/chat/chat.module": [
		699,
		7
	],
	"../pages/login/login.module": [
		700,
		6
	],
	"../pages/messages/messages.module": [
		701,
		5
	],
	"../pages/notes/notes.module": [
		702,
		4
	],
	"../pages/open-jobs/open-jobs.module": [
		703,
		3
	],
	"../pages/project-bids/project-bids.module": [
		704,
		2
	],
	"../pages/register/register.module": [
		706,
		1
	],
	"../pages/user-jobs/user-jobs.module": [
		705,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 223;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return substituteService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var substituteService = (function () {
    function substituteService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = Settings.ServerUrl;
    }
    ;
    substituteService.prototype.getAllSubstitute = function (url) {
        var body = new FormData();
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    substituteService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], substituteService);
    return substituteService;
}());

;
//# sourceMappingURL=substituteService.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddJobModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_toastService__ = __webpack_require__(85);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AddJobModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 * https://github.com/VitaliiBlagodir/cordova-plugin-datepicker
 */
var AddJobModalComponent = (function () {
    function AddJobModalComponent(viewCtrl, Toast, navParams) {
        this.viewCtrl = viewCtrl;
        this.Toast = Toast;
        this.navParams = navParams;
    }
    AddJobModalComponent.prototype.onSubmit = function (form, type) {
        if (type == 1) {
            if (form.value.date == undefined)
                this.Toast.presentToast('חובה לבחור תאריך');
            else if (form.value.time == undefined)
                this.Toast.presentToast('חובה לבחור שעה');
            else if (form.value.info == undefined)
                this.Toast.presentToast('חובה להזין תיאור המשרה');
            else {
                var data = { 'date': form.value.date, 'time': form.value.time, 'info': form.value.info, 'type': type };
                this.viewCtrl.dismiss(data);
            }
        }
        else {
            var data = { 'date': form.value.date, 'time': form.value.time, 'info': form.value.info, 'type': type };
            this.viewCtrl.dismiss(data);
        }
    };
    AddJobModalComponent.prototype.ionViewWillEnter = function () {
        this.job = this.navParams.get('job');
        this.date = this.job.Date2;
        this.time = this.job.Time2;
        this.info = this.job.info;
        console.log("TT : ", this.job);
    };
    AddJobModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'add-job-modal',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\components\add-job-modal\add-job-modal.html"*/'<!-- Generated template for the AddJobModalComponent component -->\n\n<div class="Content">\n\n  <div class="title">\n\n    הכנסת משרה\n\n  </div>\n\n\n\n    <form #f="ngForm" class="Form"  >\n\n        <ion-list>\n\n\n\n            <ion-item class="formElement">\n\n                <ion-label>בחר תאריך</ion-label>\n\n                <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="date" name="date"></ion-datetime>\n\n            </ion-item>\n\n\n\n            <ion-item class="formElement">\n\n                <ion-label>בחר שעה</ion-label>\n\n                <ion-datetime displayFormat="HH:mm " pickerFormat="HH mm " [(ngModel)]="time" name="time"></ion-datetime>\n\n            </ion-item>\n\n\n\n            <ion-item>\n\n                <ion-textarea [(ngModel)]="info" name="info" placeholder="הכניסי תיאור המשרה"></ion-textarea>\n\n            </ion-item>\n\n\n\n        </ion-list>\n\n\n\n        <div style="width: 100%; margin-top: 50px;" align="center">\n\n            <div style="width: 80%" align="center">\n\n                <button ion-button block (click)="onSubmit(f,1)">הוסף משרה </button>\n\n                <button ion-button block (click)="onSubmit(f,0)" style="background-color: red">בטל משרה </button>\n\n            </div>\n\n        </div>\n\n\n\n    </form>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\components\add-job-modal\add-job-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], AddJobModalComponent);
    return AddJobModalComponent;
}());

//# sourceMappingURL=add-job-modal.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddNoteModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_toastService__ = __webpack_require__(85);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AddJobModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 * https://github.com/VitaliiBlagodir/cordova-plugin-datepicker
 */
var AddNoteModalComponent = (function () {
    function AddNoteModalComponent(viewCtrl, navParams, Toast) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.Toast = Toast;
    }
    AddNoteModalComponent.prototype.onSubmit = function (form, type) {
        if (type == 1) {
            if (form.value.title == undefined)
                this.Toast.presentToast('חובה לבחור נושא');
            else if (form.value.info == undefined)
                this.Toast.presentToast('חובה להזין תיאור תבנית');
            else {
                var data = { 'title': form.value.title, 'info': form.value.info, 'type': type };
                this.viewCtrl.dismiss(data);
            }
        }
        else {
            var data = { 'title': form.value.title, 'info': form.value.info, 'type': type };
            this.viewCtrl.dismiss(data);
        }
    };
    AddNoteModalComponent.prototype.ionViewWillEnter = function () {
        this.info = this.navParams.get('info');
        this.title = this.navParams.get('title');
        console.log("TT : ", this.info + " : " + this.title);
    };
    AddNoteModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'add-job-modal',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\components\add-note-modal\add-note-modal.html"*/'<!-- Generated template for the AddJobModalComponent component -->\n\n<div class="Content">\n\n  <div class="title">\n\n    הכנסת תבנית\n\n  </div>\n\n\n\n    <form #f="ngForm" class="Form" >\n\n        <ion-list>\n\n            <ion-item style="text-align: right !important;">\n\n                <ion-input [(ngModel)]="title" name="title" style="text-align: right !important; direction: rtl;" placeholder="הכניסי נושא התבנית"></ion-input>\n\n            </ion-item>\n\n            <ion-item>\n\n                <ion-textarea [(ngModel)]="info" name="info" placeholder="הכניסי תיאור התבנית"></ion-textarea>\n\n            </ion-item>\n\n        </ion-list>\n\n\n\n        <div style="width: 100%; margin-top: 50px;" align="center">\n\n            <div style="width: 80%" align="center">\n\n                <button type="submit" ion-button block (click)="onSubmit(f,1)">הוסף תבנית </button>\n\n                <button ion-button block (click)="onSubmit(f,0)" style="background-color: red">בטל תבנית </button>\n\n            </div>\n\n        </div>\n\n\n\n    </form>\n\n</div>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\components\add-note-modal\add-note-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_toastService__["a" /* toastService */]])
    ], AddNoteModalComponent);
    return AddNoteModalComponent;
}());

//# sourceMappingURL=add-note-modal.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(371);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 371:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(694);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(695);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_loginService__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_config__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_all_substitute_all_substitute__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_user_jobs_user_jobs__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_messages_messages__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__ = __webpack_require__(696);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_add_job_modal_add_job_modal__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_date_picker__ = __webpack_require__(697);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_JobsService__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_register_register__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_toastService__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__services_substituteService__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_open_jobs_open_jobs__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_notes_notes__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_add_note_modal_add_note_modal__ = __webpack_require__(324);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_sent_to_server_service__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_project_bids_project_bids__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_chat_chat__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__services_chatService__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_transfer__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_file__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_path__ = __webpack_require__(323);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_camera__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_native_diagnostic__ = __webpack_require__(321);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


































var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_all_substitute_all_substitute__["a" /* AllSubstitutePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_user_jobs_user_jobs__["a" /* UserJobsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_messages_messages__["a" /* MessagesPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_16__components_add_job_modal_add_job_modal__["a" /* AddJobModalComponent */],
                __WEBPACK_IMPORTED_MODULE_24__components_add_note_modal_add_note_modal__["a" /* AddNoteModalComponent */],
                __WEBPACK_IMPORTED_MODULE_19__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_open_jobs_open_jobs__["a" /* OpenJobsPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_notes_notes__["a" /* NotesPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_project_bids_project_bids__["a" /* ProjectBidsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_chat_chat__["a" /* ChatPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], { tabsPlacement: 'top' }, {
                    links: [
                        { loadChildren: '../pages/all-substitute/all-substitute.module#AllSubstitutePageModule', name: 'AllSubstitutePage', segment: 'all-substitute', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/chat/chat.module#ChatPageModule', name: 'ChatPage', segment: 'chat', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/messages/messages.module#MessagesPageModule', name: 'MessagesPage', segment: 'messages', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notes/notes.module#NotesPageModule', name: 'NotesPage', segment: 'notes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/open-jobs/open-jobs.module#OpenJobsPageModule', name: 'OpenJobsPage', segment: 'open-jobs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/project-bids/project-bids.module#ProjectBidsPageModule', name: 'ProjectBidsPage', segment: 'project-bids', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/user-jobs/user-jobs.module#UserJobsPageModule', name: 'UserJobsPage', segment: 'user-jobs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_11__angular_http__["c" /* HttpModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_all_substitute_all_substitute__["a" /* AllSubstitutePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_user_jobs_user_jobs__["a" /* UserJobsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_messages_messages__["a" /* MessagesPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_16__components_add_job_modal_add_job_modal__["a" /* AddJobModalComponent */],
                __WEBPACK_IMPORTED_MODULE_24__components_add_note_modal_add_note_modal__["a" /* AddNoteModalComponent */],
                __WEBPACK_IMPORTED_MODULE_19__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_open_jobs_open_jobs__["a" /* OpenJobsPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_notes_notes__["a" /* NotesPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_project_bids_project_bids__["a" /* ProjectBidsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_chat_chat__["a" /* ChatPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_28__services_chatService__["a" /* ChatService */],
                __WEBPACK_IMPORTED_MODULE_9__services_loginService__["a" /* loginService */],
                __WEBPACK_IMPORTED_MODULE_18__services_JobsService__["a" /* JobsService */],
                __WEBPACK_IMPORTED_MODULE_25__services_sent_to_server_service__["a" /* sent_to_server_service */],
                __WEBPACK_IMPORTED_MODULE_21__services_substituteService__["a" /* substituteService */],
                __WEBPACK_IMPORTED_MODULE_20__services_toastService__["a" /* toastService */],
                __WEBPACK_IMPORTED_MODULE_10__services_config__["a" /* Config */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_date_picker__["a" /* DatePicker */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_33__ionic_native_diagnostic__["a" /* Diagnostic */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_file_path__["a" /* FilePath */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__all_substitute_all_substitute__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__messages_messages__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_jobs_user_jobs__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_config__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_loginService__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__open_jobs_open_jobs__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_sent_to_server_service__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = (function () {
    function HomePage(navParams, navCtrl, Settings, Login, Server) {
        var _this = this;
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.Settings = Settings;
        this.Login = Login;
        this.Server = Server;
        this.UserType = 0;
        this.SelectedIndex = 1;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__all_substitute_all_substitute__["a" /* AllSubstitutePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_4__user_jobs_user_jobs__["a" /* UserJobsPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__messages_messages__["a" /* MessagesPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_7__open_jobs_open_jobs__["a" /* OpenJobsPage */];
        if (!Settings.User) {
            this.Login.getUserById('getUserById').then(function (data) {
                Settings.User = data[0];
                _this.UserType = Settings.User.type_id == 3 || Settings.User.type_id == 4 ? 2 : 1;
                _this.SelectedIndex = _this.UserType == 2 ? 3 : 2;
                console.log(_this.UserType + " : " + _this.SelectedIndex);
            });
        }
        else {
            this.UserType = Settings.User.type_id == 3 || Settings.User.type_id == 4 ? 2 : 1;
            this.SelectedIndex = this.navParams.get('SelectedIndex');
            console.log(this.UserType + " : " + this.SelectedIndex);
        }
        this.getHomeDetails();
    }
    HomePage.prototype.getHomeDetails = function () {
        var _this = this;
        this.Server.getHomeDetails('getHomeDetails').then(function (data) {
            console.log("getHomeDetails : ", data);
            _this.Settings.JobsCount = data.jobs;
            _this.Settings.Messages = data.messages;
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\home\home.html"*/'\n\n<!--</ion-content> selectedIndex={{SelectedIndex}}-->\n\n\n\n\n\n    <ion-content padding>\n\n        <ion-tabs *ngIf="UserType == 1" selectedIndex="{{SelectedIndex}}" color="primary">\n\n            <ion-tab  [root]="tab3Root" tabTitle="הודעות" tabIcon="mail" tabIndex=0 tabBadge={{Settings.Messages}}></ion-tab>\n\n            <ion-tab  [root]="tab2Root" tabTitle="המשרות שלי" tabIndex=1 tabIcon="calendar" tabBadge={{Settings.JobsCount}}></ion-tab>\n\n            <ion-tab  [root]="tab1Root" tabTitle="מחליפות" tabIndex=2 tabIcon="people" tabBadge={{Settings.Substitute}}></ion-tab>\n\n        </ion-tabs>\n\n\n\n        <ion-tabs *ngIf="UserType == 2" selectedIndex=1 color="primary">\n\n            <ion-tab [root]="tab3Root" tabTitle="הודעות" tabIcon="mail" tabIndex = 0 tabBadge="23"></ion-tab>\n\n            <ion-tab [root]="tab4Root" tabTitle="המשרות שלי" tabIndex = 3 tabIcon="calendar" tabBadge={{Settings.openJobs}}></ion-tab>\n\n        </ion-tabs>\n\n    </ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_6__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_8__services_sent_to_server_service__["a" /* sent_to_server_service */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sent_to_server_service; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var sent_to_server_service = (function () {
    function sent_to_server_service(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = Settings.ServerUrl;
    }
    ;
    sent_to_server_service.prototype.addNote = function (url, info) {
        var body = new FormData();
        body.append('title', info["title"]);
        body.append('info', info["info"]);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("titleInfo : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.updateNote = function (url, info, id) {
        var body = new FormData();
        body.append('title', info["title"]);
        body.append('info', info["info"]);
        body.append('id', id);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("titleInfo : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.getAllNotes = function (url) {
        var body = new FormData();
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.deleteNote = function (url, id) {
        var body = new FormData();
        body.append('id', id);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    //Bid
    sent_to_server_service.prototype.addBid = function (url, job, note) {
        var body = new FormData();
        body.append('job', job);
        body.append('note', note);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.getHomeDetails = function (url) {
        var body = new FormData();
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    sent_to_server_service.prototype.updateJob = function (url, info, id) {
        var body = new FormData();
        body.append('Date2', info['date']);
        body.append('Time2', info['time']);
        body.append('info', info['info']);
        body.append('id', id);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("titleInfo : ", data); }).toPromise();
    };
    sent_to_server_service = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], sent_to_server_service);
    return sent_to_server_service;
}());

;
//# sourceMappingURL=sent_to_server_service.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_config__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_chatService__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChatPage = (function () {
    function ChatPage(navCtrl, zone, navParams, ChatService, platform, Settings, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.zone = zone;
        this.navParams = navParams;
        this.ChatService = ChatService;
        this.Settings = Settings;
        this.events = events;
        this.chatText = '';
        this.screenHeight = screen.height - 100 + 'px';
        this.ChatService._ChatArray.subscribe(function (val) {
            _this.zone.run(function () {
                _this.ChatArray = val;
            });
        });
        this.reciverId = this.navParams.get('id');
        this.reciverName = this.navParams.get('name');
        console.log("C1 : ", this.ChatService.ChatArray);
        this.phpHost = 'http://tapper.co.il/647-app/php/';
        document.addEventListener('resume', function () {
            _this.getChatDetails();
        });
        events.subscribe('newchat', function (user, time) {
            // user and time are the same arguments passed in `events.publish(user, time)`
            if (_this.content._scroll)
                _this.scrollBottom();
        });
    }
    ChatPage.prototype.ngOnInit = function () {
        this.getChatDetails();
        this.scrollBottom();
        console.log("C2 : ", this.ChatService.ChatArray);
        this.innerHeight = (window.screen.height);
        var elm = document.querySelector(".chatContent");
        elm.style.height = (this.innerHeight - (this.innerHeight * 0.23)) + 'px';
    };
    ChatPage.prototype.scrollBottom = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.content)
                _this.content.scrollToBottom(0);
        }, 300);
    };
    ChatPage.prototype.getChatDetails = function () {
        var _this = this;
        this.ChatService.getChatDetails('getChatDetails', this.reciverId).then(function (data) {
            console.log("ChatDetails: ", data), _this.ChatArray = data, _this.scrollBottom();
        });
    };
    ChatPage.prototype.ionViewDidLoad = function () {
        this.scrollBottom();
    };
    ChatPage.prototype.ionViewWillEnter = function () {
        this.scrollBottom();
    };
    ChatPage.prototype.addChatTitle = function () {
        var _this = this;
        if (this.chatText) {
            this.date = new Date();
            this.hours = this.date.getHours();
            this.minutes = this.date.getMinutes();
            this.seconds = this.date.getSeconds();
            if (this.hours < 10)
                this.hours = "0" + this.hours;
            if (this.minutes < 10)
                this.minutes = "0" + this.minutes;
            this.time = this.hours + ':' + this.minutes;
            this.today = new Date();
            this.dd = this.today.getDate();
            this.mm = this.today.getMonth() + 1; //January is 0!
            this.yyyy = this.today.getFullYear();
            if (this.dd < 10) {
                this.dd = '0' + this.dd;
            }
            if (this.mm < 10) {
                this.mm = '0' + this.mm;
            }
            this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
            this.newdate = this.today + ' ' + this.time;
            this.Obj = {
                id: '',
                sender_id: window.localStorage.id,
                title: this.chatText,
                date: this.newdate,
                reciver_id: this.reciverId
            };
            console.log(this.ChatService.ChatArray);
            this.ChatService.pushToArray(this.Obj);
            this.ChatService.addTitle('addChatTitle', this.Obj).then(function (data) {
                console.log("Weights : ", data), _this.chatText = '', _this.content.scrollToBottom();
            });
        }
    };
    ChatPage.prototype.getHour = function (DateStr) {
        var Hour = DateStr.split(" ");
        return Hour[1];
    };
    ChatPage.prototype.enterPress = function (keyEvent) {
        if (keyEvent.which === 13) {
            this.addChatTitle();
        }
    };
    ChatPage.prototype.goBack = function () {
        if (this.navParams.get('type') == 1)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], { SelectedIndex: 1 });
        else if (this.navParams.get('type') == 2)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], { SelectedIndex: 0 });
        else if (this.navParams.get('type') == 3)
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], { SelectedIndex: 2 });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
    ], ChatPage.prototype, "content", void 0);
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chat',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\chat\chat.html"*/'<!--\n\n  Generated template for the ChatPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n--><ion-header xmlns="http://www.w3.org/1999/html">\n\n\n\n  <ion-navbar hideBackButton>\n\n    <ion-title align="center" class="HeaderTitle"> צהלה <span class="HeaderSubTitle">צ\'ט</span> </ion-title>\n\n    <ion-buttons left>\n\n      <button ion-button (click)="goBack()" icon-only>\n\n        <ion-icon ios="ios-arrow-back" md="md-arrow-back"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="ContentClass">\n\n\n\n\n\n  <div class="chatContent">\n\n\n\n    <div class="chatMessage" *ngFor="let message of ChatArray">\n\n\n\n      <div class="chatMessageRight">\n\n        <img *ngIf="message.image" src="{{phpHost+message.image}}" class="imageclass"/>\n\n        <img *ngIf="!message.image" src="https://wordsmith.org/words/images/avatar2_large.png" class="imageclass"/>\n\n      </div>\n\n      <div class="chatMessageLeft">\n\n        <div class="chatTitle">\n\n          <div class="chatTitleLeft">\n\n            <p>{{getHour(message.date)}}</p>\n\n          </div>\n\n          <div class="chatTitleRight">\n\n            <p>{{reciverName}}</p>\n\n          </div>\n\n        </div>\n\n\n\n        <p class="chatMessageLeftP">{{message.title}}</p>\n\n      </div>\n\n\n\n    </div>\n\n\n\n  </div>\n\n</ion-content>\n\n\n\n\n\n<ion-footer class="footerClass">\n\n  <div class="chatFooterDiv" align="center">\n\n    <div class="chatFooterDivRight">\n\n      <ion-item align="center">\n\n        <ion-input class="chatInput" type="text" name="title" [(ngModel)]="chatText" placeholder="הוסף הודעה" (keypress)="enterPress($event)"></ion-input>\n\n      </ion-item>\n\n    </div>\n\n    <div class="chatFooterDivLeft" (click)="addChatTitle()">\n\n      <img src="images/addchat.png" class="imageclass"/>\n\n    </div>\n\n  </div>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\chat\chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_chatService__["a" /* ChatService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_config__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_notes_notes__ = __webpack_require__(165);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, Settings) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.Settings = Settings;
        this.rootPage = ""; //LoginPage;
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'עמוד ראשי', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], icon: 'ios-home-outline' },
            { title: 'מי אנחנו', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], icon: 'ios-people-outline' },
            { title: 'ערוך פרטים אישיים', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], icon: 'ios-create-outline' },
            { title: 'תבנית הערות', component: __WEBPACK_IMPORTED_MODULE_7__pages_notes_notes__["a" /* NotesPage */], icon: 'ios-document-outline' },
            { title: 'התנתק', component: 'LogOut', icon: 'ios-close-circle-outline' }
        ];
        //console.log("Local : ", window.localStorage.id, window.localStorage.sid)
        if (window.localStorage.id == undefined || window.localStorage.id == '')
            this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]; //RegisterPage;//
        else
            this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]; // ChatPage; //
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component == 'LogOut') {
            window.localStorage.id = '';
            this.Settings.User = '';
            this.nav.push(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
            console.log("Out");
        }
        else
            this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\app\app.html"*/'<ion-menu [content]="content" side="right">\n\n  <ion-header>\n\n    <ion-toolbar  style="background-color: #022850 !important;">\n\n      <ion-title style="text-align: right;">תפריט צהלה</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content>\n\n    <ion-list>\n\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" style="text-align: right; direction: rtl">\n\n        <ion-icon class="sideMenuIcon" name="{{p.icon}}"></ion-icon>\n\n        <span class="sideMenuText">{{p.title}}</span>\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false" ></ion-nav>'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_6__services_config__["a" /* Config */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 695:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\list\list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\list\list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 696:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__all_substitute_all_substitute__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__messages_messages__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_jobs_user_jobs__ = __webpack_require__(93);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = (function () {
    function TabsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__all_substitute_all_substitute__["a" /* AllSubstitutePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_4__user_jobs_user_jobs__["a" /* UserJobsPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__messages_messages__["a" /* MessagesPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',
            template: "<ion-tabs>\n                    <ion-tab [root]=\"tab1Root\" tabTitle=\"\u05DE\u05D7\u05DC\u05D9\u05E4\u05D5\u05EA\"></ion-tab>\n                    <ion-tab [root]=\"tab2Root\" tabTitle=\"\u05D4\u05DE\u05E9\u05E8\u05D5\u05EA \u05E9\u05DC\u05D9\"></ion-tab>\n                    <ion-tab [root]=\"tab3Root\" tabTitle=\"\u05D4\u05D5\u05D3\u05E2\u05D5\u05EA\" tabIcon=\"star\"></ion-tab>\n                  </ion-tabs>"
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return toastService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var toastService = (function () {
    function toastService(toastCtrl) {
        this.toastCtrl = toastCtrl;
        console.log("shay");
    }
    toastService.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom',
            cssClass: 'ToastClass'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    toastService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["p" /* ToastController */]])
    ], toastService);
    return toastService;
}());

//# sourceMappingURL=toastService.js.map

/***/ }),

/***/ 86:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var loginService = (function () {
    function loginService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = Settings.ServerUrl;
    }
    ;
    loginService.prototype.getUserDetails = function (url, info) {
        console.log("Info : ", info);
        var body = new FormData();
        body.append('mail', info["mail"]);
        body.append('password', info["password"]);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService.prototype.logOutUser = function () {
        window.localStorage.identify = '';
        this.Settings.UserId = 0;
    };
    loginService.prototype.getRegions = function (url) {
        var body = new FormData();
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Regions : ", data); }).toPromise();
    };
    loginService.prototype.GetTypes = function (url) {
        var body = new FormData();
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("Regions : ", data); }).toPromise();
    };
    loginService.prototype.RegisterUser = function (url, info) {
        var body = 'info=' + JSON.stringify(info);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.post(this.ServerUrl + '' + url, body, options).map(function (res) { return res.json(); }).do(function (data) { console.log("SaveMeals : ", data); }).toPromise();
    };
    loginService.prototype.getUserById = function (url) {
        var body = new FormData();
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], loginService);
    return loginService;
}());

;
//# sourceMappingURL=loginService.js.map

/***/ }),

/***/ 91:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllSubstitutePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_substituteService__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__chat_chat__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the AllSubstitutePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AllSubstitutePage = (function () {
    function AllSubstitutePage(app, navCtrl, navParams, substitute, Settings) {
        var _this = this;
        this.app = app;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.substitute = substitute;
        this.Settings = Settings;
        this.Substitute = [];
        this.substitute.getAllSubstitute('getAllSubstitute').then(function (data) {
            _this.Substitute = data;
            Settings.Substitute = data.length;
            console.log("Sub : ", _this.Substitute);
        });
    }
    AllSubstitutePage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad AllSubstitutePage');
    };
    AllSubstitutePage.prototype.changeTab = function () {
        //this.navCtrl.parent.select(1);
        console.log("Chat1");
    };
    AllSubstitutePage.prototype.gotoChat = function (i) {
        console.log("Chat1 : ", i, this.Substitute[i]);
        //this.navCtrl.push(ChatPage, {id: this.Substitute[i].id, name: this.Substitute[i].name, type: 3});
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_4__chat_chat__["a" /* ChatPage */], { id: this.Substitute[i].id, name: this.Substitute[i].name, type: 3 });
    };
    AllSubstitutePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-all-substitute',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\all-substitute\all-substitute.html"*/'<!--\n\n  Generated template for the AllSubstitutePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header class="Header">\n\n\n\n  <ion-navbar >\n\n    <button class="HeaderMenuButton"  ion-button icon-only menuToggle end>\n\n      <ion-icon name="md-menu"></ion-icon>\n\n    </button>\n\n    <ion-title class="HeaderTitle"> צהלה <span class="HeaderSubTitle">עמוד מחליפות</span> </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div>\n\n    <ion-list>\n\n      <div ion-item text-wrap detail-push *ngFor="let sub of Substitute let i=index" side="right" style="direction:rtl;" >\n\n        <div class="jobRight">\n\n          <img class="thumb" src="https://wordsmith.org/words/images/avatar2_large.png" style="width: 100%" />\n\n        </div>\n\n        <div class="jobLeft">\n\n          <div class="time">\n\n            <div class="timeLeft">\n\n              <h3>  {{sub.name}}</h3>\n\n            </div>\n\n          </div>\n\n          <h2>{{sub.address}}</h2>\n\n          <button ion-button (click)="gotoChat(i)">שלח הודעה ל{{sub.name}}</button>\n\n        </div>\n\n      </div>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\all-substitute\all-substitute.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_substituteService__["a" /* substituteService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]])
    ], AllSubstitutePage);
    return AllSubstitutePage;
}());

//# sourceMappingURL=all-substitute.js.map

/***/ }),

/***/ 92:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessagesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_chatService__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__chat_chat__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_config__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MessagesPage = (function () {
    function MessagesPage(app, navCtrl, navParams, ChatService, Settings) {
        var _this = this;
        this.app = app;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ChatService = ChatService;
        this.Settings = Settings;
        this.messages = [];
        this.ChatService.getMessages('getMessages').then(function (data) {
            _this.messages = data;
            _this.Settings.Messages = _this.messages.length;
            console.log("messages : ", _this.messages);
        });
        console.log('UserJobsPage');
        //getMessages
    }
    MessagesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MessagesPage');
    };
    MessagesPage.prototype.gotoChat = function (i) {
        //console.log("chat : " ,this.messages[i].id )
        //this.navCtrl.push(ChatPage, {id: this.messages[i].id, name: this.messages[i].user_name});
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_3__chat_chat__["a" /* ChatPage */], { id: this.messages[i].id, name: this.messages[i].user_name, type: 2 });
    };
    MessagesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-messages',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\messages\messages.html"*/'<!--\n\n  Generated template for the MessagesPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n  <ion-navbar >\n\n    <button class="HeaderMenuButton"  ion-button icon-only menuToggle end>\n\n      <ion-icon name="md-menu"></ion-icon>\n\n    </button>\n\n    <ion-title class="HeaderTitle"> צהלה <span class="HeaderSubTitle">עמוד הודעות</span> </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <div>\n\n    <ion-list>\n\n      <ion-item  text-wrap detail-push *ngFor="let message of messages let i=index" side="right" style="direction:rtl" >\n\n        <div class="jobRight" >\n\n          <img class="thumb" src="https://wordsmith.org/words/images/avatar2_large.png" style="width: 100%; margin-top: 0px !important;" />\n\n        </div>\n\n        <div class="jobLeft">\n\n          <div class="time">\n\n\n\n          </div>\n\n          <h2>{{message.user_name}}</h2>\n\n          <h3>מספר הודעות : {{message.messages}}</h3>\n\n          <button ion-button (click)="gotoChat(i)">שלח הודעה ל{{message.user_name}}</button>\n\n          <!--<button ion-button (click)="open_close_project(job)">{{checkState(job.is_close)}}</button>-->\n\n          <!--<button ion-button (click)="open_project_bid(i)">{{job.bids.length}} הצעות הוגשו</button>-->\n\n        </div>\n\n      </ion-item>\n\n    </ion-list>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\messages\messages.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_chatService__["a" /* ChatService */], __WEBPACK_IMPORTED_MODULE_4__services_config__["a" /* Config */]])
    ], MessagesPage);
    return MessagesPage;
}());

//# sourceMappingURL=messages.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserJobsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_add_job_modal_add_job_modal__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_JobsService__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_config__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__project_bids_project_bids__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_sent_to_server_service__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the UserJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UserJobsPage = (function () {
    function UserJobsPage(app, navCtrl, navParams, modalCtrl, jobsService, Settings, serverService) {
        var _this = this;
        this.app = app;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.jobsService = jobsService;
        this.Settings = Settings;
        this.serverService = serverService;
        this.jobs = [];
        this.jobsService.getAllJobs('getAllJobs').then(function (data) {
            _this.jobs = data;
            Settings.JobsCount = data.length;
            console.log("Jobs12 : ", _this.jobs);
        });
        console.log('UserJobsPage');
    }
    UserJobsPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad UserJobsPage');
    };
    UserJobsPage.prototype.openModal = function () {
        var _this = this;
        var jobsModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_add_job_modal_add_job_modal__["a" /* AddJobModalComponent */]);
        jobsModal.present();
        jobsModal.onDidDismiss(function (data) {
            console.log("jb : ", data);
            if (data.type == 1) {
                _this.jobsService.addJob('addJob', data).then(function (data) {
                    _this.jobs = data;
                });
            }
        });
    };
    UserJobsPage.prototype.updateModal = function (i) {
        var _this = this;
        var noteModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_add_job_modal_add_job_modal__["a" /* AddJobModalComponent */], {
            job: this.jobs[i]
        });
        noteModal.present();
        noteModal.onDidDismiss(function (data) {
            console.log(data);
            _this.serverService.updateJob('updateJob', data, _this.jobs[i]['id']).then(function (data) {
                _this.jobs = data;
            });
        });
    };
    UserJobsPage.prototype.open_close_project = function (job) {
        job.is_close == 0 ? job.is_close = 1 : job.is_close = 0;
        this.jobsService.UpdateJobState('UpdateJobState', job.id, job.is_close).then(function (data) {
            console.log("JobState : ", data);
        });
    };
    UserJobsPage.prototype.checkState = function (type) {
        var str = (type == 0) ? 'הפרוייקט פתוח' : 'הפרוייקט סגור';
        return str;
    };
    UserJobsPage.prototype.open_project_bid = function (i) {
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_5__project_bids_project_bids__["a" /* ProjectBidsPage */], { project: this.jobs[i].bids });
        //this.navCtrl.push(ProjectBidsPage,{project:this.jobs[i].bids});
    };
    UserJobsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-user-jobs',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\user-jobs\user-jobs.html"*/'<!--\n\n  Generated template for the UserJobsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header xmlns="http://www.w3.org/1999/html">\n\n\n\n  <ion-navbar >\n\n    <button class="HeaderMenuButton"  ion-button icon-only menuToggle end>\n\n      <ion-icon name="md-menu"></ion-icon>\n\n    </button>\n\n    <ion-title align="center" class="HeaderTitle"> צהלה <span class="HeaderSubTitle">המשרות שלי</span> </ion-title>\n\n    <ion-buttons>\n\n      <button ion-button icon-only (click)="openModal()" class="HeaderLeftButton">\n\n        <ion-icon name="ios-add-circle-outline"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n    <div>\n\n        <ion-list>\n\n          <ion-item  text-wrap detail-push *ngFor="let job of jobs let i=index" side="right" style="direction:rtl">\n\n              <div class="jobRight">\n\n                  <img class="thumb" src="https://wordsmith.org/words/images/avatar2_large.png" style="width: 100%" />\n\n              </div>\n\n              <div class="jobLeft">\n\n                  <div class="time">\n\n                      <div class="timeLeft">\n\n                          <h3> תאריך : {{job.Date2}}</h3>\n\n                      </div>\n\n                      <div class="timeRight">\n\n                          <h3 style="direction: rtl">שעה : {{job.Time2}}</h3>\n\n                      </div>\n\n                      <div class="edit" (click)="updateModal(i)">\n\n                          <img src="images/edit.png" style="width: 100%" />\n\n                      </div>\n\n                  </div>\n\n                  <hr style="margin:0px;">\n\n                  <h2>{{job.info}}</h2>\n\n                  <button ion-button (click)="open_close_project(job)">{{checkState(job.is_close)}}</button>\n\n                  <button ion-button (click)="open_project_bid(i)" style="background-color: red">{{job.bids.length}} הצעות הוגשו</button>\n\n              </div>\n\n          </ion-item>\n\n        </ion-list>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\user-jobs\user-jobs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ModalController */], __WEBPACK_IMPORTED_MODULE_3__services_JobsService__["a" /* JobsService */], __WEBPACK_IMPORTED_MODULE_4__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_6__services_sent_to_server_service__["a" /* sent_to_server_service */]])
    ], UserJobsPage);
    return UserJobsPage;
}());

//# sourceMappingURL=user-jobs.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_loginService__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__register_register__ = __webpack_require__(164);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, Login, Settings) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Login = Login;
        this.Settings = Settings;
        this.info = { mail: "haya@gmail.com", password: '123' };
        this.user = '';
        console.log(this.info["mail"]);
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.checkLogin = function () {
        var _this = this;
        console.log(this.info);
        this.Login.getUserDetails('getUserDetails', this.info).then(function (data) {
            console.log("UserDetails : ", data[0]);
            _this.user = data[0];
            _this.setUserSettings(data);
        });
    };
    LoginPage.prototype.setUserSettings = function (data) {
        console.log("MMM : ", this.user["mail"]);
        window.localStorage.id = this.user["id"];
        this.Settings.UserConnected = this.user;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    LoginPage.prototype.GoToRegister = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__register_register__["a" /* RegisterPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\login\login.html"*/'\n\n<ion-content padding>\n\n  <div class="loginMain">\n\n    <div class="loginTitle">\n\n      התחברות\n\n    </div>\n\n    <div class="InputsText" align="center">\n\n      <div class="InputText">\n\n        <input type="text" placeholder="הכנס מייל תקין" [(ngModel)]="info.mail" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס סיסמה" [(ngModel)]="info.password" />\n\n      </div>\n\n\n\n      <button class="loginButton" ion-button block (click)="checkLogin()">התחל</button>\n\n      <a class="register" (click)="GoToRegister()">הרשמ/י למערכת</a>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Documents\github\gan\gan_app12\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[366]);
//# sourceMappingURL=main.js.map