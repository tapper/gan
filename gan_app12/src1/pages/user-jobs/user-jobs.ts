import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {AddJobModalComponent} from "../../components/add-job-modal/add-job-modal";
import {JobsService} from "../../services/JobsService";
import {Config} from "../../services/config";

/**
 * Generated class for the UserJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-user-jobs',
    templateUrl: 'user-jobs.html',
})
export class UserJobsPage {

    public jobs:any[] = [];

    constructor(public navCtrl: NavController, public navParams: NavParams , public modalCtrl: ModalController, public jobsService:JobsService , public Settings:Config) {
        this.jobsService.getAllJobs('getAllJobs').then((data: any) => {
            this.jobs = data;
            Settings.JobsCount = data.length;
            console.log("Jobs : " , this.jobs)
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad UserJobsPage');
    }

    openModal() {
        let jobsModal = this.modalCtrl.create(AddJobModalComponent);
        jobsModal.present();

        jobsModal.onDidDismiss(data => {
            console.log("jb : " , data);

            this.jobsService.addJob('addJob',data).then((data: any) => {

            });
        });
    }

    open_close_project(job)
    {
        job.is_close == 0 ?  job.is_close = 1 :  job.is_close = 0;

        this.jobsService.UpdateJobState('UpdateJobState',job.id,job.is_close).then((data: any) => {
            console.log("JobState : " , data)
        });
    }

    checkState(type)
    {
        let str = (type == 0) ? 'הפרוייקט פתוח' : 'הפרוייקט סגור';
        return str;
    }

}
