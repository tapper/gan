import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {JobsService} from "../../services/JobsService";
import {Config} from "../../services/config";

/**
 * Generated class for the OpenJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-open-jobs',
  templateUrl: 'open-jobs.html',
})
export class OpenJobsPage {

  public jobs;

  constructor(public navCtrl: NavController, public navParams: NavParams , public jobsService:JobsService , public Settings:Config ) {
      this.jobsService.getAllOpenJobs('getAllOpenJobs').then((data: any) => {
          this.jobs = data;
          Settings.openJobs = data.length;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpenJobsPage');
  }


}
