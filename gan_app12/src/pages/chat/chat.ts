import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Content} from "ionic-angular";
import {Platform} from 'ionic-angular';
import {Config} from "../../services/config";
import {ChatService} from "../../services/chatService";
import {HomePage} from "../home/home";
import {MessagesPage} from "../messages/messages";

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
})
export class ChatPage {
    @ViewChild(Content) content: Content;

    public chatText = '';
    public ChatArray;
    Obj: any;
    public screenHeight = screen.height - 100 + 'px';
    public date: any;
    public hours: any;
    public minutes: any;
    public seconds: any;
    public time: any;
    public today: any;
    public dd: any;
    public mm: any;
    public yyyy: any;
    public newdate: any;
    public phpHost;
    innerHeight: any;
    public reciverId: number;
    public reciverName: number;

    constructor(public navCtrl: NavController, public zone: NgZone, public navParams: NavParams, public ChatService: ChatService, platform: Platform, public Settings: Config, public events: Events) {
        this.ChatService._ChatArray.subscribe(val => {
            this.zone.run(() => {
                this.ChatArray = val;
            });
        });

        this.reciverId = this.navParams.get('id');
        this.reciverName = this.navParams.get('name');

        console.log("C1 : ", this.ChatService.ChatArray);

        this.phpHost = 'http://tapper.co.il/647-app/php/';
        document.addEventListener('resume', () => {
            this.getChatDetails();
        });

        events.subscribe('newchat', (user, time) => {
            // user and time are the same arguments passed in `events.publish(user, time)`
            if (this.content._scroll)
                this.scrollBottom();
        });
    }


    ngOnInit() {
        this.getChatDetails();
        this.scrollBottom();
        console.log("C2 : ", this.ChatService.ChatArray);
        this.innerHeight = (window.screen.height);
        let elm = <HTMLElement>document.querySelector(".chatContent");
        elm.style.height = (this.innerHeight - (this.innerHeight * 0.23)) + 'px'
    }

    scrollBottom() {
        setTimeout(() => {
            if (this.content)
                this.content.scrollToBottom(0);
        }, 300);
    }

    getChatDetails() {
        this.ChatService.getChatDetails('getChatDetails', this.reciverId).then((data: any) => {
            console.log("ChatDetails: ", data), this.ChatArray = data , this.scrollBottom();
        });
    }

    ionViewDidLoad() {
        this.scrollBottom();
    }

    ionViewWillEnter() {
        this.scrollBottom();
    }

    addChatTitle() {
        if (this.chatText) {
            this.date = new Date()
            this.hours = this.date.getHours()
            this.minutes = this.date.getMinutes()
            this.seconds = this.date.getSeconds()

            if (this.hours < 10)
                this.hours = "0" + this.hours

            if (this.minutes < 10)
                this.minutes = "0" + this.minutes
            this.time = this.hours + ':' + this.minutes;


            this.today = new Date();
            this.dd = this.today.getDate();
            this.mm = this.today.getMonth() + 1; //January is 0!
            this.yyyy = this.today.getFullYear();

            if (this.dd < 10) {
                this.dd = '0' + this.dd
            }

            if (this.mm < 10) {
                this.mm = '0' + this.mm
            }

            this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
            this.newdate = this.today + ' ' + this.time;


            this.Obj = {
                id: '',
                sender_id: window.localStorage.id,
                title: this.chatText,
                date: this.newdate,
                reciver_id: this.reciverId
            };
            console.log(this.ChatService.ChatArray)
            this.ChatService.pushToArray(this.Obj);

            this.ChatService.addTitle('addChatTitle', this.Obj).then((data: any) => {
                console.log("Weights : ", data), this.chatText = '', this.content.scrollToBottom();
            });
        }
    }

    getHour(DateStr) {
        let Hour = DateStr.split(" ");
        return Hour[1];
    }

    enterPress(keyEvent) {
        if (keyEvent.which === 13) {
            this.addChatTitle();
        }
    }


    goBack() {
        if (this.navParams.get('type') == 1)
            this.navCtrl.push(HomePage, {SelectedIndex: 1});
        else if(this.navParams.get('type') == 2)
            this.navCtrl.push(HomePage, {SelectedIndex: 0});
        else if(this.navParams.get('type') == 3)
        this.navCtrl.push(HomePage, {SelectedIndex: 2});
    }

}
