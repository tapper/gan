import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {loginService} from "../../services/loginService";
import {Config} from "../../services/config";
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})


export class LoginPage {

    public info: Object = {mail: "haya@gmail.com", password: '123'};
    public user:string = '';

    constructor(public navCtrl: NavController, public navParams: NavParams , public Login:loginService , public Settings:Config) {
        console.log(this.info["mail"])
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }

    checkLogin() {
        console.log(this.info);
        this.Login.getUserDetails('getUserDetails',this.info).then(
            (data: any) => {console.log("UserDetails : " , data[0]);
            this.user = data[0];
            this.setUserSettings(data);
            });
    }

    setUserSettings(data)
    {
        console.log("MMM : " , this.user["mail"]);
        window.localStorage.id = this.user["id"];
        this.Settings.UserConnected = this.user;
        this.navCtrl.push(HomePage);
    }

    GoToRegister()
    {
        this.navCtrl.push(RegisterPage);
    }
}
