import {Component} from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ChatService} from "../../services/chatService";
import {ChatPage} from "../chat/chat";
import {Config} from "../../services/config";

/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-messages',
    templateUrl: 'messages.html',
})
export class MessagesPage {

    public messages: any[] = [];

    constructor(public app: App , public navCtrl: NavController, public navParams: NavParams, public ChatService: ChatService , public Settings: Config) {
        this.ChatService.getMessages('getMessages').then((data: any) => {
            this.messages = data;
            this.Settings.Messages = this.messages.length;
            console.log("messages : ", this.messages)
        });
        console.log('UserJobsPage');
        //getMessages
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad MessagesPage');
    }

    gotoChat(i) {
        //console.log("chat : " ,this.messages[i].id )
        //this.navCtrl.push(ChatPage, {id: this.messages[i].id, name: this.messages[i].user_name});
        this.app.getRootNav().setRoot(ChatPage,{id: this.messages[i].id, name: this.messages[i].user_name , type:2});
    }

}
