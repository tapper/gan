import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {JobsService} from "../../services/JobsService";
import {Config} from "../../services/config";
import {sent_to_server_service} from "../../services/sent_to_server_service";

/**
 * Generated class for the OpenJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-open-jobs',
    templateUrl: 'open-jobs.html',
})
export class OpenJobsPage {

    public jobs;
    public CurrentJob = '';
    public Notes: any[] = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public jobsService: JobsService, public Settings: Config, public serverService: sent_to_server_service, private alertCtrl: AlertController) {
        this.jobsService.getAllOpenJobs('getAllOpenJobs').then((data: any) => {
            console.log("Jobs : " ,data )
            this.jobs = data;
            Settings.openJobs = data.length;
        });

        this.serverService.getAllNotes('getAllNotes').then((data: any) => {
            this.Notes = data;
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad OpenJobsPage');
    }

    presentPrompt(i) {
        console.log("JB " , i , this.jobs[i].id)
        this.CurrentJob = this.jobs[i].id;
        let myAlert = this.alertCtrl.create(
            {
                title: "בחרי תבנית",
                cssClass : 'noteAlert',
                buttons: [
                    {
                        text: 'בטלי',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'הגישי הצעה',
                        handler: (data) => {
                            this.serverService.addBid('addBid',this.CurrentJob,this.Notes[data].id ).then((data: any) => {
                                this.jobs[i].isBid = 1
                            });
                        }
                    }
                ]
            }
        );

        for (let i = 0; i < this.Notes.length; i++) {
            myAlert.addInput({
                type: 'radio',
                label: this.Notes[i]['title'],
                value: i.toString()
            })
        }
        myAlert.present();
    }

}
