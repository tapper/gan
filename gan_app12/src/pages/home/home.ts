import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {AllSubstitutePage} from "../all-substitute/all-substitute";
import {MessagesPage} from "../messages/messages";
import {UserJobsPage} from "../user-jobs/user-jobs";
import {Config} from "../../services/config";
import {loginService} from "../../services/loginService";
import {OpenJobsPage} from "../open-jobs/open-jobs";
import {sent_to_server_service} from "../../services/sent_to_server_service";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {
    public UserType = 0;
    public SelectedIndex:number = 1;
    tab1Root = AllSubstitutePage;
    tab2Root = UserJobsPage;
    tab3Root = MessagesPage;
    tab4Root = OpenJobsPage;

    constructor(public navParams: NavParams , public navCtrl: NavController, public Settings: Config, public Login: loginService, public Server:sent_to_server_service) {
        if (!Settings.User) {
            this.Login.getUserById('getUserById').then(
                (data: any) => {
                    Settings.User = data[0];
                    this.UserType = Settings.User.type_id == 3 || Settings.User.type_id == 4 ? 2 : 1;
                    this.SelectedIndex = this.UserType == 2 ? 3 : 2;
                    console.log(this.UserType + " : "  + this.SelectedIndex)
                });
        }
        else
        {
            this.UserType = Settings.User.type_id == 3 || Settings.User.type_id == 4 ? 2 : 1;
            this.SelectedIndex = this.navParams.get('SelectedIndex');
            console.log(this.UserType  + " : "  + this.SelectedIndex)
        }

        this.getHomeDetails();
    }

    getHomeDetails()
    {
        this.Server.getHomeDetails('getHomeDetails').then(
            (data: any) => {
                console.log("getHomeDetails : " , data)
                this.Settings.JobsCount = data.jobs;
                this.Settings.Messages = data.messages;
            });
    }

}
