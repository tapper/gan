import {Component} from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {substituteService} from "../../services/substituteService";
import {Config} from "../../services/config";
import {ChatPage} from "../chat/chat";

/**
 * Generated class for the AllSubstitutePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-all-substitute',
    templateUrl: 'all-substitute.html',
})
export class AllSubstitutePage {

    public Substitute: any[] = [];

    constructor(public app: App , public navCtrl: NavController, public navParams: NavParams, public substitute: substituteService, public Settings: Config) {
        this.substitute.getAllSubstitute('getAllSubstitute').then((data: any) => {
            this.Substitute = data;
            Settings.Substitute = data.length;
            console.log("Sub : " , this.Substitute)
        });
    }

    ionViewDidLoad() {
        // console.log('ionViewDidLoad AllSubstitutePage');
    }

    changeTab() {
        //this.navCtrl.parent.select(1);
        console.log("Chat1")

    }

    gotoChat(i) {
        console.log("Chat1 : " , i , this.Substitute[i])
        //this.navCtrl.push(ChatPage, {id: this.Substitute[i].id, name: this.Substitute[i].name, type: 3});
        this.app.getRootNav().setRoot(ChatPage,{id: this.Substitute[i].id, name: this.Substitute[i].name, type: 3});
    }

}
