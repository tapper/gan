import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectBidsPage } from './project-bids';

@NgModule({
  declarations: [
    ProjectBidsPage,
  ],
  imports: [
    IonicPageModule.forChild(ProjectBidsPage),
  ],
})
export class ProjectBidsPageModule {}
