import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserJobsPage} from "../user-jobs/user-jobs";
import {HomePage} from "../home/home";
import {ChatPage} from "../chat/chat";


/**
 * Generated class for the ProjectBidsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-project-bids',
  templateUrl: 'project-bids.html',
})


export class ProjectBidsPage {

  public Bids:any[]= [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.Bids = this.navParams.get('project');
      console.log(this.Bids)
      console.log('ProjectBidsPage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectBidsPage');
  }

    goBack()
    {
        this.navCtrl.push(HomePage,{SelectedIndex:1});
    }

    gotoChat(i)
    {
        this.navCtrl.push(ChatPage,{id:this.Bids[i].user[0].id, name:this.Bids[i].user[0].name , type:1});
    }
}
