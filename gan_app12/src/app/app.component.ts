import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {LoginPage} from "../pages/login/login";
import {RegisterPage} from "../pages/register/register";
import {Config} from "../services/config";
import {NotesPage} from "../pages/notes/notes";
import {ChatPage} from "../pages/chat/chat";

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = "";//LoginPage;

    pages: Array<{ title: string, component: any , icon:string }>;

    constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen , public Settings:Config) {
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            {title: 'עמוד ראשי', component: HomePage , icon:'ios-home-outline'},
            {title: 'מי אנחנו', component: HomePage , icon:'ios-people-outline'},
            {title: 'ערוך פרטים אישיים', component: HomePage , icon:'ios-create-outline'},
            {title: 'תבנית הערות', component: NotesPage , icon:'ios-document-outline'},
            {title: 'התנתק', component: 'LogOut' , icon:'ios-close-circle-outline'}
        ];

        //console.log("Local : ", window.localStorage.id, window.localStorage.sid)
        if (window.localStorage.id == undefined || window.localStorage.id == '')
            this.rootPage = LoginPage; //RegisterPage;//
        else
            this.rootPage = HomePage; // ChatPage; //
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component == 'LogOut') {
            window.localStorage.id = '';
            this.Settings.User = '';
            this.nav.push(LoginPage);
            console.log("Out")
        }
        else
            this.nav.setRoot(page.component);
    }
}
