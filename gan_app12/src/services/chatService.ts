import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable, ViewChild} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import {Content} from "ionic-angular";


//const ServerUrl = "http://tapper.co.il/647/laravel/public/api/";


@Injectable()

export class ChatService
{
    public ServerUrl;
    ChatArray:any[] = [];
    public _ChatArray = new Subject<any>();
    ChatArray$: Observable<any> = this._ChatArray.asObservable();

    @ViewChild(Content) content: Content;

    constructor(private http:Http,public Settings:Config) {
        this.ServerUrl = Settings.ServerUrl;
        console.log(this.ChatArray)
    }

    addTitle(url:string , obj)
    {
        let body = 'info=' + JSON.stringify(obj);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.ServerUrl + '' + url, body, options).map(res => res.json()).do((data)=>{console.log("Chat : " , data)}).toPromise();
    }

    getChatDetails(url:string,reciverId)
    {
        let body = new FormData();
        body.append('sender_id', window.localStorage.id );
        body.append('reciver_id', reciverId );
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.updateChatArray(data)}).toPromise();
    }

    updateChatArray(data)
    {
        this.ChatArray = data;
        //this._ChatArray.next({ text: this.ChatArray });
    }

    pushToArray(data)
    {
         this.ChatArray.push(data);
         console.log("Push1 : " , data)
         this._ChatArray.next(this.ChatArray);
    }

    registerPush(url,pushid)
    {
        let body = new FormData();
        body.append('push_id', pushid.toString() );
        body.append('user_id', window.localStorage.identify.toString() );
        return this.http.post(this.ServerUrl + '' + url, body).map(res => '').do((data)=>{console.log("chat:" , data)});
    }

    getChatMessagesCount(url)
    {
        let body = new FormData();
        body.append('user_id', window.localStorage.identify.toString() );
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{return data;}).toPromise();
    }

    getMessages(url)
    {
        let body = new FormData();
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{return data;}).toPromise();
    }
};


