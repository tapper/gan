
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {HomePage} from "../pages/home/home";


@Injectable()

export class JobsService
{
    public ServerUrl;

    constructor(private http:Http,public Settings:Config) {
        this.ServerUrl = Settings.ServerUrl;
    };

    addJob(url:string , info:string)
    {
        let body = 'uid=' + window.localStorage.id + '&info=' + JSON.stringify(info);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        let options = new RequestOptions({
            headers: headers
        });

        return this.http.post(this.ServerUrl + '' + url, body, options).map(res => res.json()).do((data)=>{console.log("SaveMeals : " , data) }).toPromise();
    }

    getAllJobs(url:string)
    {
        let body = new FormData();
        body.append('id',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{ }).toPromise();
    }

    getAllOpenJobs(url:string)
    {
        let body = new FormData();
        body.append('id',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{ }).toPromise();
    }

    UpdateJobState(url:string , id , state)
    {
        let body = new FormData();
        body.append('id',id);
        body.append('state',state);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }

};


