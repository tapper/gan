import {Component} from '@angular/core';
import {NavParams, ViewController} from "ionic-angular";
import {DatePicker} from "@ionic-native/date-picker";
import {toastService} from "../../services/toastService";

/**
 * Generated class for the AddJobModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 * https://github.com/VitaliiBlagodir/cordova-plugin-datepicker
 */
@Component({
    selector: 'add-job-modal',
    templateUrl: 'add-job-modal.html'
})

export class AddJobModalComponent {

    public job;
    public date;
    public time;
    public info;

    constructor(public viewCtrl: ViewController, public Toast:toastService , private navParams: NavParams) {
    
    }
    
    onSubmit(form , type)
    {
        if(type == 1)
        {
            if(form.value.date == undefined)
                this.Toast.presentToast('חובה לבחור תאריך');
            else if(form.value.time == undefined)
                this.Toast.presentToast('חובה לבחור שעה');
            else if(form.value.info == undefined)
                this.Toast.presentToast('חובה להזין תיאור המשרה');
            else
            {
                let data = {'date': form.value.date , 'time' : form.value.time , 'info' : form.value.info , 'type' : type};
                this.viewCtrl.dismiss(data);
            }
        }
        else
        {
            let data = {'date': form.value.date , 'time' : form.value.time , 'info' : form.value.info , 'type' : type};
            this.viewCtrl.dismiss(data);
        }
    }

    ionViewWillEnter() {
        this.job = this.navParams.get('job');
        this.date = this.job.Date2;
        this.time = this.job.Time2;
        this.info = this.job.info;
        console.log("TT : " , this.job )
    }
}
